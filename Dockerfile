FROM node:alpine

COPY package.json .
COPY config_entitlement.yaml .
COPY config_user.yaml .
COPY config.js .
RUN mkdir handlers
COPY handlers/ ./handlers

RUN npm install

# Expose port
EXPOSE 8081
EXPOSE 8002

# Start the app
CMD ["npm", "start"]
