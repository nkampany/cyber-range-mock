let JsonResponse = require('configurapi-handler-json').JsonResponse;
let ErrorResponse = require('configurapi').ErrorResponse;
let Config = require('../config');

const userWithoutCanViewCourseware = {
    "createdTimestamp": "2019-01-25T15:14:01.599Z",
    "email": "user.three@domain.tld",
    "id": "apiunit_ui_org1_user3_id",
    "identityProvider": "basic",
    "name": "User Three",
    "resources": [
      {
        "id": "any",
        "role": "user",
        "type": "any"
      },
      {
        "id": "apiunit_ui_org1_course1_id",
        "role": "student",
        "type": "course"
      },
      {
        "id": "apiunit_ui_org1",
        "role": "organizationstudent",
        "type": "organization",
        "businessUnit": "school1",
        "businessRegion": "school_district1",
        "businessCategory": "K12",
        "title": "first_grader"
      }
    ]
  };

const userWithCanViewCoursewareUnderResourceScope = {
  "createdTimestamp": "2019-01-25T15:14:01.531Z",
  "email": "user.one@domain.tld",
  "id": "apiunit_ui_org1_user1_id",
  "identityProvider": "basic",
  "name": "User One",
  "resources": [
    {
      "id": "any",
      "role": "user",
      "type": "any"
    },
    {
      "id": "apiunit_ui_org1_course1_id",
      "role": "instructor",
      "type": "course"
    },
    {
      "id": "apiunit_ui_org1",
      "role": "organizationinstructor",
      "type": "organization",
      "businessUnit": "school1",
      "businessRegion": "school_district1",
      "businessCategory": "K12",
      "title": "math_teacher"
    }
  ]
}

const userWithCanViewCoursewareUnderAnyScope = {
    "createdTimestamp": "2019-01-25T15:14:01.406Z",
    "email": "super.admin@domain.tld",
    "id": "superadmin",
    "identityProvider": "basic",
    "name": "super admin",
    "resources": [
      {
        "id": "any",
        "role": "user",
        "type": "any"
      },
      {
        "id": "any",
        "role": "superadmin",
        "type": "organization"
      }
    ]
  };

const userWithMultipleCanViewCourseware = {
    "createdTimestamp": "2019-01-25T15:14:01.614Z",
    "email": "user.four@domain.tld",
    "id": "apiunit_ui_org1_user4_id",
    "identityProvider": "basic",
    "name": "User Four",
    "resources": [
      {
        "id": "any",
        "role": "user",
        "type": "any"
      },
      {
        "id": "apiunit_ui_org1_course1_id",
        "role": "student",
        "type": "course"
      },
      {
        "id": "apiunit_ui_org1_course2_id",
        "role": "instructor",
        "type": "course"
      },
      {
        "id": "apiunit_ui_org1",
        "role": "organizationstudent",
        "type": "organization",
        "businessUnit": "school1",
        "businessRegion": "school_district1",
        "businessCategory": "K12",
        "title": "first_grader"
      },
      {
        "id": "apiunit_ui_org2",
        "role": "organizationinstructor",
        "type": "organization",
        "businessUnit": "school2",
        "businessRegion": "school_district2",
        "businessCategory": "college",
        "title": "professor"
      }
    ]
  }

module.exports = function getUserHandler(event)
{
    let user = undefined;
    
    let token = event.request.headers['authorization'];

    //Token 1 - A user who does not have a can_view_courseware claim.
    if(token === Config.userWithoutCanViewCoursewareToken)
    {
        user =  userWithoutCanViewCourseware;
    }

    //Token 2 - A user who has a can_view_courseware claim under a scope.
    else if(token === Config.userWithCanViewCoursewareUnderResourceScopeToken)
    {
        user =  userWithCanViewCoursewareUnderResourceScope;
    }
    //Token 3 - A user who has a can_view_courseware under any scope.
    else if(token === Config.userWithCanViewCoursewareUnderAnyScopeToken)
    {
        user = userWithCanViewCoursewareUnderAnyScope;
    }

    //Token 4 - A user who has multiple can_view_courseware claims
    else if(token === Config.userWithMultipleCanViewCoursewareToken)
    {
        user = userWithMultipleCanViewCourseware;
    }
    else
    {
        event.response = new ErrorResponse('Forbidden', 403);
        return this.complete();
    }

    event.response = new JsonResponse(user, 200);
};